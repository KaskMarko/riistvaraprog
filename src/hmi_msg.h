
#include <avr/pgmspace.h>
#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_

#define PRO1 "Version: "
#define PRO2 " built on: "
#define LIBC "avr-libc version: "
#define AVR "avr-gcc version: "
#define STUD_NAME "Marko Kask"
#define MONTH "Enter Month name first letter >"
#define UPTIME "Uptime: %lu s"
#define UART_STATUS_MASK 0xFF
#define CMD_ERROR "\nCommand not implemented\n Use <help> to get help.\n"
#define CMD_ARG_ERROR "\nToo few or too many arguments for this command.\nUse <help>\n"
#define HELP1 "Implemented commands:\n"
#define HELP2 " : "
#define ACCESS "ACCESS DENIED!"

extern PGM_P const name_month[];

#endif
