
#ifndef _CLI_MICRORL_H_
#define _CLI_MICRORL_H_

void print (const char * str);
char cli_get_char(void);
void cli_print_help(void);
void cli_print_ver(void);
void cli_print_ascii_tbls(void);
void cli_handle_month(const char *const *argv);
void cli_print_cmd_error(void);
void cli_print_cmd_arg_error(void);
void read(void);
void add(const char *const *argv);
void remove(const char *const *argv);
void list(void);
int cli_execute(int argc, const char *const *argv);

#endif
