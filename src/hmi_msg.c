
#include <avr/pgmspace.h>
#include "hmi_msg.h"

const char s1[] PROGMEM = "January";
const char s2[] PROGMEM = "February";
const char s3[] PROGMEM = "March";
const char s4[] PROGMEM = "April";
const char s5[] PROGMEM = "May";
const char s6[] PROGMEM = "June";

PGM_P const name_month[] PROGMEM = {s1, s2, s3, s4, s5, s6};
